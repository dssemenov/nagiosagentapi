****************************************
Nagios-Agent
****************************************

This agent runs on Nagios host sampling data from logs and transforming the data
to JSON data structure

Nagios collector service will consume this agent's API to feed data into mongoDB
for further processing
