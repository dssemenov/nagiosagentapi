"""
A place to keep general purpose functions, classes, etc
"""
from hashlib import sha1
import re


def get_hash_code(value, length=12):
    """
    Generates a SHA-1 hash based on input value and returns the hash code
    """
    if not isinstance(value, basestring):
        raise ValueError('value must be a string')
    hsh = sha1()
    hsh.update(value)
    return hsh.hexdigest()[-length:]


def do_logical_equality_search(val, data):
    """
    Performs series of regex searches on data and places matches on
    local stack, then popping them out until match object is present
    """
    matches = []
    for token in val.split('|'):
        match = re.search(token, data, re.IGNORECASE)
        matches.append(match)
    for match in matches:
        if match is not None:
            return True
    return False


def op_compare(optr, data, val):
    """
    Performs comparisson based search on given operator
    """
    data = str(data) # convert to string as sometimes date int may come in
    is_logical_search = True if '|' in val else False

    if optr == "$ne":
        if is_logical_search:
            return not do_logical_equality_search(val, data)
        else:
            match = re.search(val, data, re.IGNORECASE)
            return True if match is None else False
    elif optr == "$gt":
        return int(data) > int(val)
    elif optr == "$lt":
        return int(data) < int(val)
    else:
        if is_logical_search:
            return do_logical_equality_search(val, data)
        else:
            match = re.search(val, data, re.IGNORECASE)
            return True if match is not None else False
