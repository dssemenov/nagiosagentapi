"""
Defines query API for filtering and manipulating response JSON that agent produce
"""
#author: dsemenov
import simplejson
from utils import op_compare, get_hash_code


class Query(object):
    """
    Defines methods to filter, aggregate and project JSON response object data
    """
    def __init__(self):
        pass


    @staticmethod
    def limit(req, arr):
        """
        Parses query parameters from request context and limits the number of
        elements in returned array
        """
        if req.args.get('limit'):
            limit = int(req.args.get('limit'))
            arr = arr[-limit:]
        return arr


    @staticmethod
    def filter(req, arr):
        """
        Release: V2
        Parses multi filter object from query parameter in the request context
        and returns a filtered array of data
        """
        if req.args.get('filter'):
            flt = simplejson.loads(req.args.get('filter').strip())
            try:
                for field in flt:
                    optr = "$eq"
                    value = flt[field] # field value to search for
                    if isinstance(value, dict):
                        optr = value.keys()[0] # equality comparer key
                        value = value[optr] # update to the new value
                    if '.' in field:
                        parent, child = field.split('.')
                        arr = [x for x in arr if parent in x
                               if op_compare(optr, x[parent][child], value)]
                    else:
                        arr = [x for x in arr if field in x
                               if op_compare(optr, x[field], value)]
            except KeyError:
                pass
        return arr


    @staticmethod
    def group(req, arr, **kwargs):
        """
        Release: V2
        Groups common records that contain duplicate field values in the
        grouping aggregate array passed via ?group query
        """
        if req.args.get('group'):
            grp = simplejson.loads(req.args.get('group').strip())
            object_cache = kwargs['object_cache']

            # iterate from the end of a list to get the most recent time record
            for elem in reversed(arr):
                dtl = elem['detail']
                tmp_id = get_hash_code('_'.join([dtl[x] for x in grp if x in dtl]))
                # check if cache is missing an entry and add
                if tmp_id not in object_cache:
                    object_cache[tmp_id] = {
                        "warden": {
                            "deleted": False
                        },
                        "node": elem
                    }
                elif elem['time'] < object_cache[tmp_id]['node']['time']:
                    continue
                else:
                    elem['_id'] = tmp_id
                    object_cache[tmp_id]['node'] = elem
            # build a tree array of all objects expect those marked for deletion in warden api
            return [v['node'] for k, v in object_cache.iteritems() if not v['warden']['deleted']] # pylint: disable=unused-variable
        return arr
