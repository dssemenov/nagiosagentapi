"""
Defines functionility that enables extraction of nagios feed
"""
#author: dsemenov
from os import environ as env
from operator import add
import re
from utils import get_hash_code


class NagiosAgent(object):
    """
    Nagios agent works by iterating nagios log and status flat files and
    build a JSON tree for extraction
    """
    def __init__(self):
        pass


    @staticmethod
    def convert_log_to_object(log_entry_type, object_keys):
        """
        General purpose function to retrieve all data records from a nagios log
        and generate object array

        log_entry_type  - type of log entry to search for in the nagios.log
        object_keys     - resulting json object keys to define custom struct
        """
        with open(env.get('NAGIOS_AGENT_LOG', '/var/log/nagios3/nagios.log')) as log:
            obj = []
            for line in log:
                line = line.strip()
                matched = re.search(r'{0}'.format(log_entry_type), line)
                if matched is None:
                    continue
                else:
                    # work the data and extract important elements
                    data = re.split(r' {0}:'.format(log_entry_type), line)
                    time = int(data[0][1:-1])
                    info = data[1].strip()
                    detail = dict(zip(object_keys, info.split(';')))
                    tmp_id = get_hash_code("{0}_{1}".format(detail['fabric'], str(time)))
                    obj.append({"_id": tmp_id, "time": time, "type": matched.group(),
                                "detail": detail})
            return obj


    @staticmethod
    def convert_data_to_object():
        """
        General purpose function to retrieve all data from status.dat file
        """
        with open(env.get('NAGIOS_STATUS_DAT', '/var/cache/nagios3/status.dat')) as log:
            obj = []
            for line in log:
                line = line.strip()
                matched_id = re.match(r"(?:\s*define)?\s*(\w+)\s+{", line)
                matched_attr = re.match(r"\s*(\w+)(?:=|\s+)(.*)", line)
                matched_end_attr = re.match(r"\s*}", line)

                if len(line) == 0 or line[0] == '#':
                    pass
                elif matched_id:
                    identifier = matched_id.group(1)
                    cur = [identifier, {}]
                elif matched_attr:
                    attribute = matched_attr.group(1)
                    value = matched_attr.group(2).strip()
                    cur[1][attribute] = value
                elif matched_end_attr and cur:
                    obj.append(cur)
                    del cur
            return reduce(add, obj)[1::2] # flattens the array obj
