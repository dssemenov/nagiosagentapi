"""
Agent runs as flask application collecting data from nagios files
"""
#author: dsemenov
from os import environ as env
import simplejson
from flask import Flask, request, Response
from flask_restful import Api
from utils import get_hash_code
from nagios import NagiosAgent
from querydsl import Query


APP = Flask(__name__)
API = Api(APP)

SERVICE_ALERTS_CACHE = {}
SERVICE_ALERT_KEYS = ['service', 'fabric', 'alert', 'level', 'state', 'info']
SERVICE_STATE_CACHE = {}
SERVICE_STATE_KEYS = ['fabric', 'state', 'level', 'count', 'info']
SERVICE_HOST_STATE_CACHE = {}
SERVICE_HOST_STATE_KEYS = ['service', 'fabric', 'state', 'task', 'info']

STATUS_DATA_FILTER_KEYS = ['last_check', 'current_state', 'host_name',
                           'check_command', 'service_description', 'plugin_output',
                           'notifications_enabled', 'last_notification',
                           'problem_has_been_acknowledged']

def get_all_alerts():
    """
    Retrieves a list of all SERVICE NOTIFICATION alerts and turn them to
    JSON struct
    """
    return NagiosAgent.convert_log_to_object('SERVICE NOTIFICATION', SERVICE_ALERT_KEYS)


def get_host_states():
    """
    Retrieves the status / state for all nagios monitored hosts
    """
    return NagiosAgent.convert_log_to_object('CURRENT HOST STATE', SERVICE_STATE_KEYS)


def get_current_host_nofitications():
    """
    Retrieves the current failure status for all nagios hosts
    """
    return NagiosAgent.convert_log_to_object('HOST NOTIFICATION', SERVICE_HOST_STATE_KEYS)


def hide_or_purge_cache_records(cache, purge=False):
    """
    Iterates a list of cache records and does hide or purge on dict keys
    """
    if purge:
        for key in cache.keys():
            if cache[key]['warden']['deleted']:
                del cache[key]
    else:
        for key in cache.keys():
            cache[key]['warden']['deleted'] = True


def has_purge_flag():
    """
    Checks if request query has ?purge param
    """
    return True if request.args.get('purge') is not None else False


@APP.after_request
def after_request(response):
    """
    This handler adds CORS headers to all API resources upon a request
    """
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@APP.route('/api/1.0/agent/alerts')
def get_alerts_depricated():
    """
    @Depricated: V1.1
    """
    alerts = get_all_alerts()
    alerts = Query.limit(request, Query.filter(request, alerts))
    for alt in alerts:
        if 'service' in alt['detail']:
            del alt['detail']['service']
        if 'level' in alt['detail']:
            del alt['detail']['level']
        if 'state' in alt['detail']:
            del alt['detail']['state']
    return simplejson.dumps(alerts, indent=4, sort_keys=False)


@APP.route('/api/1.0/agent/hosts')
def get_hosts_depricated():
    """
    @Depricated: V1.1
    """
    states = get_host_states()
    states = Query.limit(request, Query.filter(request, states))
    return simplejson.dumps(states, indent=4, sort_keys=False)


@APP.route('/api/1.0/agent/hosts/current')
def get_current_hosts_depricated():
    """
    @Depricated: V1.1
    """
    curr_states = NagiosAgent.convert_data_to_object()
    curr_states = Query.limit(request, Query.filter(request, curr_states))
    return simplejson.dumps(curr_states, indent=4, sort_keys=False)


@APP.route('/api/1.0/agent/services')
def get_current_services_depricated():
    """
    @Depricated: V1.1
    """
    services = get_current_host_nofitications()
    services = Query.limit(request, Query.filter(request, services))
    return simplejson.dumps(services, indent=4, sort_keys=False)


@APP.route('/api/1.1/agent/alerts', methods=['GET', 'DELETE'])
def get_or_delete_alerts():
    """
    Gathers all host notifications (aka alerts) from nagios.log and sends back
    data in JSON format limiting the output to required fields (see below)

    Provides a handler for DELETE call processing that iterates alerts cache and
    hides all alerts by switching the deleted key to True

    When ?purge query parameter is passed in the request this method will
    permanently delete all records that we set to True
    """
    if request.method == 'DELETE':
        hide_or_purge_cache_records(SERVICE_ALERTS_CACHE, has_purge_flag())
        return Response(status=204)
    else:
        alerts = get_all_alerts()
        alerts = Query.limit(request, Query.group(request,
                                                  Query.filter(request, alerts),
                                                  object_cache=SERVICE_ALERTS_CACHE))
        for alt in alerts:
            if 'service' in alt['detail']:
                del alt['detail']['service']
            if 'level' in alt['detail']:
                del alt['detail']['level']
            if 'state' in alt['detail']:
                del alt['detail']['state']
        return simplejson.dumps(alerts, indent=4, sort_keys=False)


@APP.route('/api/1.1/agent/alerts/<string:_id>', methods=['POST'])
def post_alerts(_id):
    """
    Handles POST events for alerts API
    """
    if _id in SERVICE_ALERTS_CACHE:
        SERVICE_ALERTS_CACHE[_id]['warden']['deleted'] = True
        return simplejson.dumps(SERVICE_ALERTS_CACHE[_id], indent=4, sort_keys=False)
    else:
        return simplejson.dumps({})


@APP.route('/api/1.1/agent/hosts', methods=['GET', 'DELETE'])
def get_hosts():
    """
    Gathers all host state information from the nagios.log and responds in JSON
    listing records collected by Nagios at midnight (aka initial service check)
    Handles HTTP DELETE by hiding or purging cache records
    """
    if request.method == 'DELETE':
        hide_or_purge_cache_records(SERVICE_STATE_CACHE, has_purge_flag())
        return Response(status=204)
    else:
        states = get_host_states()
        states = Query.limit(request, Query.group(request,
                                                  Query.filter(request, states),
                                                  object_cache=SERVICE_STATE_CACHE))
        return simplejson.dumps(states, indent=4, sort_keys=False)


@APP.route('/api/1.1/agent/services')
def get_current_services():
    """
    Gathers all current service and host state information from the nagios.log
    and sends back JSON data. The information gathered represents all services
    or hosts in DOWN status
    Handles HTTP DELETE by hiding or purging cache records
    """
    if request.method == 'DELETE':
        hide_or_purge_cache_records(SERVICE_HOST_STATE_CACHE, has_purge_flag())
        return Response(status=204)
    else:
        services = get_current_host_nofitications()
        services = Query.limit(request, Query.group(request,
                                                    Query.filter(request, services),
                                                    object_cache=SERVICE_HOST_STATE_CACHE))
        return simplejson.dumps(services, indent=4, sort_keys=False)


@APP.route('/api/1.1/agent/hosts/current')
def get_current_hosts():
    """
    Gathers all current hosts information from the status.dat log and
    presents data in a shortened JSON response structure with references to
    notifications (aka alerts)
    """
    curr_states = NagiosAgent.convert_data_to_object()
    curr_states = Query.limit(request, Query.filter(request, curr_states))
    filtered_obj = []
    for state in curr_states:
        obj = {}
        detail = {k: state[k] for k in STATUS_DATA_FILTER_KEYS if k in state}
        if detail:
            # add unique _id
            tmp_id = get_hash_code("{0}_{1}".format(detail['host_name'],
                                                    str(detail['last_check'])))
            obj['_id'] = tmp_id

            # update time to be int
            obj['time'] = int(detail['last_check'])
            del detail['last_check']

            # update the state field
            detail['current_state'] = "UP" if detail['current_state'] == "0" else "DOWN"

            # change data types for json output
            detail['notifications_enabled'] = bool(int(detail['notifications_enabled']))
            detail['problem_has_been_acknowledged'] = bool(int(
                detail['problem_has_been_acknowledged']))
            detail['last_notification'] = int(detail['last_notification'])

            obj['detail'] = detail
            filtered_obj.append(obj)
    return simplejson.dumps(filtered_obj, indent=4, sort_keys=False)


if __name__ == "__main__":
    APP.run(host='0.0.0.0', port=int(env.get('NAGIOS_AGENT_PORT', '5000')),
            debug=True, threaded=True)
