"""
Setup configuration file for pip install command
"""
from setuptools import setup, find_packages

setup(
    name='Nagios-Agent',
    version='0.1.4',
    url='http://www.actiance.com',
    author='Dmitri Semenov',
    author_email='dsemenov@actiance.com',
    description='Agent that transforms nagios log data into JSON format',
    long_description=__doc__,
    packages=['agentapi'],
    include_package_data=True,
    zip_safe=False,
    install_requires=find_packages()
)
